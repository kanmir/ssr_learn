import React from 'react';
import { connect } from 'react-redux';

import { fetchUsers } from '../actions';

class UsersList extends React.Component {
  componentDidMount() {
    this.props.fetchUsers();
  }

  render() {
    return (
      <div>
        {this.renderUsers()}
      </div>
    );
  }

  renderUsers() {
    return (
      <ul>Users:
        {this.props.users.map(user => <li key={user.id}>{user.name}</li>)}
      </ul>
    );
  }
}

const mapStateToProps = state => {
  return { users: state.users };
}

function loadData(store) {
  return store.dispatch(fetchUsers());
}

export { loadData };
export default connect(mapStateToProps, { fetchUsers })(UsersList);
